<%-- 
    Document   : index
    Created on : 10-05-2020, 3:42:55
    Author     : kevin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html style="background: #dbdbdb">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.2/css/bulma.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
        <title>OXFORD</title>
    </head>
    <body>
        <h1 style="font-size: 45px">Evaluación Final. Kevin Nava</h1>
        <nav class="navbar" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item">
      <img src="./Logo/logo.png" width="50%">
    </a>

    <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>
</nav>
        
         <form name="form" action="controller" method="POST">
                      <input class="input is-rounded" style="width: 37%; top: 8px" type="text" name="palabra" required id="palabra" placeholder="Escriba una Palabra">
                    <div class="control">
                        <button class="button is-large" style="position: relative;
                            left: 474px;
                            top: -39px;" type="submit">
                        <span class="icon">
                          <i class="fas fa-search-plus"></i>
                        </span>
                      </button>
                    </div>
                </div>
         </form>
        <footer style="position: relative;
            font-size: 25px;
            top: 325px;
            left: 460px;">Taller de Aplicaciones Empresariales</footer>
         
    </body>
</html>
