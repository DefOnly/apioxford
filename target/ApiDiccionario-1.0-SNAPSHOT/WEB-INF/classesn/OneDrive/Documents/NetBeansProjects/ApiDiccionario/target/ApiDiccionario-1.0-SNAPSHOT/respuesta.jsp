<%-- 
    Document   : respuesta
    Created on : 10-05-2020, 5:55:45
    Author     : kevin
--%>

<%@page import="java.util.Iterator"%>
<%@page import="root.model.entities.Palabra"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Palabra> p = (List<Palabra>) request.getAttribute("lista");
    Iterator<Palabra> iter = p.iterator();
%>
<html style="background: #dbdbdb">
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
         <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.2/css/bulma.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultado</title>
    </head>
    <body>
        <div class="container">
            <h2><strong>RESPUESTA DICCIONARIO OXFORD:</strong></h2><br><hr>
            <div class="form-group">
                <label for="rut" class="control-label col-sm-2">Palabra:</label>
                <div class="col-sm-15">
                    <input name="palabra" value="<%= request.getAttribute("palabra")%>" disabled class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="nombre" class="control-label col-sm-2">Significado:</label>
                <div class="col-sm-15">
                    <textarea name="significado" rows="2" cols="50" disabled class="form-control">
                        <%=request.getAttribute("significado").toString().trim()%>
                    </textarea>
                </div>
            </div> 
        </div>
        <br>
        <button class="button is-success is-focused" style="position: relative;
            left: 46%;" onclick="history.back()">Volver</button>
        
        <div class="container">
            <h2><strong>HISTORIAL DE BÚSQUEDA</strong></h2><br><hr>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Palabra</th>    
                        <th>Significado</th>           
                    </tr>
                </thead>
                <tbody>                    
                    <%
                        while (iter.hasNext()) {
                            Palabra word = iter.next();
                    %>
                        <td><%= word.getPalabra()%></td>
                        <td><%= word.getSignificado()%></td>
                    </tr>
                    <%
                      }
                    %>                    
                </tbody>
            </table>
        </div>
    </body>
</html>
