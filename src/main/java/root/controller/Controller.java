/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import root.dao.entities.PalabraDAO;
import root.model.entities.Palabra;

/**
 *
 * @author kevin
 */
@WebServlet(name = "Controller", urlPatterns = {"/controller"})
public class Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Controller</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PalabraDAO dao = new PalabraDAO();
        final String json;
        String palabra = request.getParameter("palabra");
        
        //Api Rest Local
        Client client = ClientBuilder.newClient();
        WebTarget myResource = client.target("http://LAPTOP-OS5GM7KI:8080/ApiDiccionario-1.0-SNAPSHOT/api/diccionario/" +palabra);
        json = myResource.request(MediaType.APPLICATION_JSON).header("api-key", "18e44d2baf87da20e41bbf5971d83d2a").header("api-id", "fed0b528").get(String.class);
        
        JSONParser parser = new JSONParser();
        String ret = "404";
        try
        {
            final String result = json;
            final Object parse = parser.parse(result);
            ret = (String) ((JSONArray) ((JSONObject) ((JSONArray) ((JSONObject) ((JSONArray) ((JSONObject) 
                    ((JSONArray) ((JSONObject) ((JSONArray) ((JSONObject) parse)
                    .get("results")).get(0)).get("lexicalEntries")).get(0)).get("entries"))
                    .get(0)).get("senses")).get(0)).get("definitions")).get(0);
        } catch (org.json.simple.parser.ParseException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("resultado desde JSON : " +ret);
       
        Palabra pab = new Palabra();
        pab.setPalabra(palabra);        
        if(!"404".equals(ret)){
            pab.setSignificado(ret);
        }
        else{
            pab.setSignificado("No Hay Resultados");
            ret = pab.getSignificado();
        }        
        dao.create(pab);
        
        //Lista de Consultas
        List<Palabra> p = dao.findPalabraEntities();

        request.setAttribute("palabra", palabra);
        request.setAttribute("significado", ret);
        request.setAttribute("lista", p);
        request.getRequestDispatcher("respuesta.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
     private void assertEquals(String string, String property) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void assertNull(String property) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
